
/*********
**********
**********					BY jaysir@163.com   2015.06.19
**********					
**********					depend: session.(key) ; key defined at req(app.use("/login"))
**********
**********								
**********					
**********			
**********					发送给用户错误级别：0：注销登陆		    销毁session 回退登陆页
**********										1：发送错误提示     不影响其他
**********										0：重新登录
**********										0：重新登录
**********										0：重新登录
**********/
var io =  require("socket.io").listen("3030");
var privateType = "prm";
function chat(mongoStore){
 
	if(this.constructor!=chat){return new chat(mongoStore);}//console.log(this);
	var self = this;
	this.mongoStore = mongoStore;	
	//	mongoStore = new require('connect-mongo')(session);在app.use(session({Store:mongoStore}))前定义
	this.maxUid  = 0;	//  在线人数			
	
	this.connMap = {};	//  在线用户Socket存放   email => socket 注：email唯一标识，且在用户列表里标识好友
	
	this.userInf = {};  //  用户信息   email => inf 待定  暂未使用

	this.discussGroup = {} //临时讨论组	key => gid 待定 暂未使用

	this.io = io;		//  服务端Socket变量
	
	//	获取session信息  session变量名依赖于用户登录后存储的变量名
	this.getSessionEmail = function(err,session){
		userEmail = session.userEmail;
	}
	//处理用户登陆
	this.addUser = function(data){
		if(connMap[userEmail] != undefined){						//已登录用户不可再登录
		 	connection.emit("err",{"msg":"用户已登陆！","err":0});	//发送错误信息，错误级别0；
		 	return;
		}
		addUser = true;												//标记用户登录
		self.maxUid ++;												//记录在线用户数量
		self.connMap[userEmail] = connection;						//在线用户连接信息
		connection.broadcast.emit("ilogin",{"email":userEmail});	//广播发送好友上线

		// TODO从数据库查询离线消息  返回给用户
	}
	//处理用户退出
	this.disconnect = function(socket){
		if(addUser == true){
			addUser = false;
			connection.broadcast.emit("ileft",{"email":userEmail});
			delete connMap[userEmail];
			maxUid --;
		}
	}

	//处理用户发送消息
	this.sendMsg = function(data){
		/*		接收data 结构:
		*			{
						"msgType":"prm"			//prm :privatemsg私信	
						"to" :  				// 私信为email
						"msg":msgBody;
		*			}
				转发data 结构：
					{
						"msgType":"prm"			//prm :privatemsg私信	
						"from" :  				// 私信为email
						"msg":msgBody;
					}
		*/
		if(data.msgType == privateType){
			if(connMap[data.to]==undefined){		//用户不在线
				//  TODO   聊天信息存放数据库	标记未读信息
				return;
			}
			//  TODO   聊天信息存放数据库

			self.connMap[data.to].emit(data.msgType,{"msgType":privateType,"from":userEmail,"msg":data.msg});
		}
	}




	this.io.on("connection",function(connection){
		var addUser = false;
		var tS = cookie.parse(connection.handshake.headers.cookie)['connect.sid'];
 		var sessionID = tS.split(".")[0].split(":")[1];
		var userEmail ;
		self.mongoStore.get(sessionID,self.getSessionEmail);	//对userEmail 赋值

		connection.on("addUser",self.addUser);
		connection.on("sendMsg",self.sendMsg);
		connection.on("disconnect",self.disconnect);

	});
}
module.exports = chat;