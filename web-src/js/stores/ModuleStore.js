var inherits = require('inherits');

var BaseStore = require('./BaseStore');
var ModuleConstant = require('../constants/ModuleConstant');

var RouterContainer = require('../services/RouterContainer');

var ModuleStore = function(){
    var that = this;
    BaseStore.call(this);
    this.subscribe(function(){return that._registerToActions.bind(that);});
    this.currentModule = null;
};

inherits(ModuleStore, BaseStore);

ModuleStore.prototype._registerToActions = function(action) {
    switch(action.actionType) {
        case ModuleConstant.events.CHANGE_CURRENT_MODULE:
            this.currentModule = ModuleConstant.modules[action.id];
            this.emitChange();
            break;
        default:
            break;
    }
};

ModuleStore.prototype.getCurrentModule = function(){
    if(this.currentModule === null){
        var moduleId = RouterContainer.getRouter().getCurrentPath().substring(1);
        if(moduleId === ''){
            moduleId = 'home';
        }
        this.currentModule = ModuleConstant.modules[moduleId];
    }
    return this.currentModule;
};

module.exports = new ModuleStore();